
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(self) -> u32 {
        self.width * self.height
    }
    fn width(&self) -> u32 {
        self.width * 10
    }
}

#[derive(Debug, PartialEq)]
enum IpAddrKind {
    V4(u8, u8, u8, u8),
    V6(String),
}

impl std::fmt::Display for IpAddrKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            IpAddrKind::V4(a, b, c, d) => write!(f, "(V4): {}, {}, {}, {}", a, b, c, d),
            IpAddrKind::V6(a) => write!(f, "(V6): {}", a),
        }
    }
}

fn route(ip_kind: IpAddrKind) -> IpAddrKind {
    if matches!(ip_kind, IpAddrKind::V4(_, _, _, _)) {
        IpAddrKind::V6(String::from("v4"))
    } else {
        IpAddrKind::V4(1,2,3,4)
    }
}

fn main() {
    /*
    let four = IpAddrKind::V4(1,2,3,4);
    let six = IpAddrKind::V6(String::from("somestring"));

    println!("{}", route(four));
    println!("{}", route(six));
    */

    let some_number = Some(5);
    let some_char = Some('e');
    let absent_number: Option<i32> = None;

    let mut five = Some(5);
    five = plus_one(five);
    match five {
        Some(five) => println!("{}", five),
        None => (),

    }
    

}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}
