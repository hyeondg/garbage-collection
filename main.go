package main

import (
	"fmt"
	"os"
)

//var a [5]int
//for i, num := range nums {
//kvs := map[string]string{"a": "apple", "b": "banana"}
//for k, v := range kvs {
//for k := range kvs {
//for i, c := range "go" {

type Person struct {
	name   string
	age    int
	friend *Person
}

type Dict struct {
	plist []*Person
	pid   map[string]int
}

func NewDict() *Dict {
	ret := Dict{}
	ret.plist = []*Person{}
	ret.pid = map[string]int{}
	return &ret
}

func newPerson(n string) *Person {
	p := Person{}
	p.name = n
	p.age = 42
	p.friend = nil
	return &p
}

func addPerson(d *Dict, p *Person) {
	var new_id int = len(d.plist)
	d.plist = append(d.plist, p)
	d.pid[p.name] = new_id
}

func connect(d *Dict, n1 string, n2 string) {
	i1, ok := d.pid[n1]
	if !ok {
		fmt.Printf("no key named %s\n", n1)
		os.Exit(100)
	}
	i2, ok := d.pid[n2]
	if !ok {
		fmt.Printf("no key named %s\n", n2)
		os.Exit(200)
	}

	f1 := d.plist[i1]
	f2 := d.plist[i2]
	f1.friend = f2
	f2.friend = f1
}

func main() {
	d := NewDict()
	p1 := Person{name: "Alice", friend: nil, age: 20}
	p2 := newPerson("Bob")

	addPerson(d, &p1)
	addPerson(d, p2)

	connect(d, "Alice", "Bob")

	fmt.Printf("%v\n", d.plist[d.pid["Alice"]].friend.name)
	connect(d, "Alice", "Charlie")
}
