#include <iostream>
#include <memory>
#include <optional>
#include <utility>
#include <vector>
using namespace std;
/*
template <typename T>
struct Wrapper {
  constexpr Wrapper() = default;
  constexpr Wrapper(Wrapper const&) = default;
  constexpr Wrapper(T const& t)
      : t(t) {}
  constexpr T get() const {
    return t;
  }
  constexpr bool operator==(Wrapper const& rhs) const = default;

private:
  T t;
};

struct Day {
  constexpr Day()
      : x(0), y(0) {}
  constexpr Day(int x)
      : x(x), y(0) {}
  constexpr Day(int x, int y)
      : x(x), y(y) {}
  constexpr bool operator==(Day const& rhs) const = default;

private:
  int x;
  int y;
};
*/

enum Flags {
  good = 0,
  fail,
  bad,
  eof = 4
};

/*
constexpr int operator|(Flags f1, Flags f2) {
  return Flags(int(f1) | int(f2));
}

constexpr void f(Flags f3) {
  constexpr int x2 = bad | f3;
  cout << x2 << endl;
}

int main() {
  f(good);
}
*/
int GLOB = 10;
struct Foo {
private:
  int x;
  std::string name;
  static int no_name_cnt;

public:
  Foo(const Foo& f, std::optional<std::string>&& str);
  Foo(const Foo& f, std::string&& str);
  Foo(const Foo&& f, std::optional<std::string>&& str);
  Foo(std::optional<std::string>&& str);
  void quack(const std::string& name);
};

// DEFINITION
//

Foo::Foo(const Foo& f, std::optional<std::string>&& str) {
  if (str) {
    cout << "copied with name" << endl;
    this->name = *str;
  } else {
    cout << "copied without name" << endl;
    this->name = "noname" + to_string(no_name_cnt++);
  }
}

Foo::Foo(const Foo& f, std::string&& str) {
  cout << "copied with name, not optional" << endl;
  this->name = std::move(str);
}

Foo::Foo(const Foo&& f, std::optional<std::string>&& str) {
  if (str) {
    cout << "moved" << endl;
    this->name = *str;
  } else {
    cout << "moved without name" << endl;
    this->name = "noname" + to_string(no_name_cnt++);
  }
}

Foo::Foo(std::optional<std::string>&& str) {
  if (str) {
    cout << "constructed" << endl;
    this->name = *str;
  } else {
    cout << "constructed without name" << endl;
    this->name = "noname" + to_string(no_name_cnt++);
  }
}

void Foo::quack(const std::string& name) {
  cout << "quack " << name << endl;
}

int Foo::no_name_cnt = 1;

[[nodiscard]] int do_something() {
  return 1;
}
struct A {
  int x;
  int* p;

  A() {
    *p = x;
  }
  A(const A&) = delete;
  // A(A&&) = default;
};

int main() {
  // auto a = A();
  const A a = A();

  auto u = std::make_unique<A>(std::move(a));
  // A b(std::move(a));
}
